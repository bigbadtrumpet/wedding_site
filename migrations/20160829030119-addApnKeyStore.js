'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function (db, callback) {
  db.createTable('apn_key_store', {
    id: {
      type: 'string',
      primaryKey: true,
      notNull: true
    },
    time_submitted: 'timestamp'
  }, callback);
};

exports.down = function(db, callback) {
  db.dropTable('apn_key_store', callback);
};
