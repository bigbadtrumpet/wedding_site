'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function (db, callback) {
  db.createTable('guest', {
    id: {
      type: 'int',
      primaryKey: true,
      autoIncrement: true,
      notNull: true
    },
    title: 'string',
    first_name: 'string',
    last_name: 'string',
    email: 'string',
    is_going: 'boolean',
    invite_id: {
      type: 'int',
      foreignKey: {
        name: 'invite_id_refs_invite',
        table: 'invite',
        rules: {
          onDelete: 'CASCADE',
          onUpdate: 'RESTRICT'
        },
        mapping: 'id'
      }
    }
  }, callback);
}

exports.down = function(db, callback) {
  db.dropTable('guest', callback);
};
