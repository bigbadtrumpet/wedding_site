var express = require('express');
var app = express();
var compression = require('compression');
var bodyParser = require('body-parser');

var favicon = require('serve-favicon');
app.use(compression());
app.use(bodyParser.json());
app.use(favicon(__dirname + '/public/favicon.ico'));
app.set('port', (process.env.PORT || 5000));
app.use(express.static(__dirname + '/public'));

// views is directory for all template files
app.set('views', __dirname + '/views');
app.set('images', __dirname + '/images');
app.set('view engine', 'ejs');

require('./helper/db').init(app);
require('./helper/routes').init(app, require('./helper/auth.js').auth);
app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});
