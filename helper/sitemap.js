var sitemap = require('sitemap').createSitemap ({
      hostname: 'http://tarynandcolton.com',
      cacheTime: 600000,        // 600 sec - cache purge period
      urls: [
        { url: '/',  changefreq: 'daily', priority: 0.3 },
        { url: '/travel',  changefreq: 'daily', priority: 0.3 },
        { url: '/rsvp',  changefreq: 'daily', priority: 0.3 },
        { url: '/details',  changefreq: 'daily', priority: 0.3 },
        { url: '/registry',  changefreq: 'daily', priority: 0.3 },
        { url: '/contact', changefreq: 'daily', priority: 0.3 }
      ]
    });

module.exports = {
  init:function(app){
    app.get('/sitemap.xml', function(req, res) {
      sitemap.toXML( function (err, xml) {
          if (err) {
            return res.status(500).end();
          }
          res.header('Content-Type', 'application/xml');
          res.send( xml );
      });
    });
  }
};
