var db = require('./db');
var sendgrid = require('./sendgrid');
var utils = require('./utils');

var defaultResponse = function(res, err, info){
  if(err){
    res.status(500).send({error:err});
    return;
  }
  res.send(info);
};

module.exports = {
  init: function(app, auth)
  {
    app.get('/', function(request, response) {
      response.render('pages/index');
    });

    app.get('/details', function(request, response) {
      var info = { weather_link: utils.get_weather_link()};
      response.render('pages/wedding_info', info);
    });

    app.get('/travel', function(request, response) {
      response.render('pages/travel');
    });

    app.get('/rsvp', function(request, response) {
      response.redirect('/details');
    });

    app.get('/registry', function(request, response) {
      response.render('pages/registry');
    });

    app.get('/contact', function(request, response) {
      response.render('pages/contact');
    });
    app.post('/contact', function(request, response) {
      sendgrid.sendMessage(request, response, {error:'pages/contact', redirectPath:'/details'});
    });
    app.get('/guest_management', auth, function(req, res){
      res.render('pages/guest_management');
    });
    app.get('/api/v1/summary', auth, function(req, res){
      db.summary(req, function(err, info){
        defaultResponse(res, err, info);
      });
    });
    app.get('/api/v1/guest', auth, function(req, res){
      db.list_guests(req, req.query, function(err, info){
        defaultResponse(res, err, info);
      });
    });
    app.put('/api/v1/guest/:id', auth, function(req, res){
      db.update_guest(req, req.params.id, req.body, function(err, info){
        defaultResponse(res, err, info);
      });
    });
    app.delete('/api/v1/guest/:id', auth, function(req, res){
      db.delete_guest(req, req.params.id, function(err, info){
        defaultResponse(res, err, info);
      })
    });
    app.post('/api/v1/invite', auth, function(req, res){
      db.create_invite(req, req.body, function(err, info){
        defaultResponse(res, err, info);
      });
    });
    app.get('/api/v1/invite', auth, function(req, res){
      db.list_invites(req, function(err, info){
        defaultResponse(res, err, info);
      })
    });
    app.delete('/api/v1/invite/:id', auth, function(req, res){
      db.delete_invite(req, req.params.id, function(err, info){
        defaultResponse(res, err, info);
      });
    });
    app.get('/api/v1/invite/:id', auth, function(req, res){
      db.get_invite_info(req, req.params.id, function(err, info){
        defaultResponse(res, err, info);
      });
    });
    app.get('/api/v1/invite/:id/guest', auth, function(req, res){
      db.get_invite_guests(req, req.params.id, function(err, info){
        defaultResponse(res, err, info);
      });
    });
    app.get('/api/v1/guest_summary', function(req, res){
      db.guest_summary(req, function(err, info){
        defaultResponse(res, err, info);
      });
    });
    app.post('/api/v1/invite/:id/guest', auth, function(req, res){
      db.create_invite_guest(req, req.params.id, req.body, function(err, info){
        defaultResponse(res, err, info);
      });
    });
    app.post('/api/v1/check_code', function(req, res){
      db.check_code(req, req.body, function(err, info){
        defaultResponse(res, err, info);
      });
    });
    app.post('/api/v1/submit_rsvp', function(req, res){
      db.submit_rsvp(req, req.body, function(err, info){
        defaultResponse(res, err, info);
      });
    });
    app.post('/api/v1/apn/add_token/:id', function(req, res){
      db.add_apn_token(req, req.params.id, function(err, info){
        defaultResponse(res, err, info);
      })
    })
  }
};
