exports.get_weather_link = function () {
  var now = new Date(Date.now());
  var day = now.getDate();
  var month = now.getMonth();
  var wedding_day = 21;
  if(day < wedding_day && month <= 0) {
    //add one more day to make number work
    return "http://www.accuweather.com/en/us/key-west-fl/33040/evening-weather-forecast/332323?day=" + (wedding_day - day + 1);
  } else {
    return "http://www.accuweather.com/en/us/key-west-fl/33040/weather-forecast/332323"
  }
};