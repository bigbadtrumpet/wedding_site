var qs = require('querystring');
var sendgrid  = require('sendgrid')(process.env.SENDGRID_API);
var validator = require('validator');

module.exports = {
  sendMessage : function(request, response, paths){
  var body = '';
  request.on('data', function (data) {
      body += data;
      // Too much POST data, kill the connection!
      // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
      if (body.length > 1e6)
          request.connection.destroy();
  });

  request.on('end', function () {
      var post = qs.parse(body);
      var fromEmail = post['email'];
      var name = post['name'];
      var message = post['message'];
      if(!validator.isEmail(fromEmail) || validator.isNull(name) || validator.isNull(message))
      {
        response.render(paths.errorPath, {error: true});
        return;
      }

      sendgrid.send({
        to:       'gilbertwhite.wedding@gmail.com',
        from:     fromEmail,
        subject:  'Message from ' + name,
        text:     message
      },
      function(err, json) {
        if (err) {
          return console.error(err);
        }
        console.log(json);
        response.redirect(paths.redirectPath);
      });
  });
  }
}
