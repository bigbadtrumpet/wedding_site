const Pool = require('pg-pool');
const url = require('url');
const util = require('util');

const init = function (app) {
  const params = url.parse(process.env.DATABASE_URL);
  const auth = params.auth.split(':');

  const config = {
    user: auth[0],
    password: auth[1],
    host: params.hostname,
    port: params.port,
    database: params.pathname.split('/')[1]
  };
  app.pool = new Pool(config);
};
const list_guests = function (req, params, callback) {
  console.log(params);
  var command = 'SELECT * FROM guest ';
  //noinspection JSUnresolvedVariable
  if (params != null && typeof params.invited !== 'undefined' && params.invited == 'false') {
    console.log('only getting non invites');
    command += 'WHERE invite_id IS NULL '
  }
  command += 'ORDER BY last_name ASC';
  req.app.pool.query(command, function (err, result) {
    if (err) {
      console.error(err);
      callback(err, null);
      return;
    }
    callback(null, result.rows);
  });
};
const get_guest = function (req, id, callback) {
  req.app.pool.query('SELECT * from guest WHERE id=($1)', [id], function (err, result) {
    if (err) {
      callback(err);
      return;
    }
    callback(null, result.rows[0]);
  })
};
const update_guest = function (req, id, name, callback) {
  if (typeof id === 'undefined') {
    console.log(id);
    callback('can not add undefined id');
    return;
  }
  if (typeof name === 'undefined' ||
    typeof name.first_name === 'undefined' ||
    typeof name.last_name === 'undefined' ||
    typeof name.title === 'undefined') {
    console.log(name);
    callback('can not add missing data');
    return;
  }
  req.app.pool.query("UPDATE guest SET title=($1), first_name=($2), last_name=($3) WHERE id=($4)",
    [name.title, name.first_name, name.last_name, id], function (err) {
      if (err) {
        console.error(err);
        callback(err, null);
        return;
      }
      console.log('updated id ' + id);
      get_guest(req, id, callback);
    });
};
const delete_guest = function (req, id, callback) {
  if (typeof id === 'undefined') {
    console.log(id);
    callback('can not add undefined id');
    return;
  }
  console.log('devaring guest id ' + id);
  req.app.pool.query("DEvarE FROM guest WHERE id=($1) RETURNING invite_id", [id], function (err, result) {
    if (err) {
      console.error(err);
      callback(err, null);
      return;
    }
    console.log('devared guest id ' + id);
    get_invite_guests(req, result.rows[0].invite_id, callback);
  });
};
const create_invite = function (req, data, callback) {
  if (typeof data === 'undefined' ||
    typeof data.name === 'undefined' ||
    data.name === null ||
    typeof data.code === 'undefined' ||
    data.code === null) {
    callback('can not add missing key parts of information');
    return;
  }
  req.app.pool.query("INSERT INTO invite(name, code) VALUES ($1, $2)", [data.name, data.code], function (err) {
    if (err) {
      console.log('error creating invite id');
      console.log(err);
      callback(err);
      return;
    }
    list_invites(req, callback);
  });
};
const list_invites = function (req, callback) {
  req.app.pool.query('SELECT * FROM invite ORDER BY name ASC', function (err, result) {
    if (err) {
      console.error(err);
      callback(err, null);
      return;
    }
    callback(null, result.rows);
  });
};
const delete_invite = function (req, id, callback) {
  if (typeof id === 'undefined') {
    console.log(id);
    callback('can not devare undefined id');
    return;
  }
  req.app.pool.query('DEvarE FROM invite WHERE id=($1)', [id], function () {
    list_invites(req, callback);
  });
};
const get_invite_info = function (req, id, callback) {
  req.app.pool.query('select  from guest, invite where invite.id = ($1) AND guest.invite_id=($1)', [id], function (err, result) {
    if (err) {
      callback(err);
      return;
    }
    callback(null, result.rows);
  })
};
const get_invite_guests = function (req, id, callback) {
  req.app.pool.query('select * from guest where invite_id=($1)', [id], function (err, result) {
    if (err) {
      callback(err);
    }
    if (typeof result == 'undefined' || result == null) {
      callback({error: 'Database returned null for id ' + id});
      return;
    }
    callback(null, result.rows);
  })
};
const create_invite_guest = function (req, id, data, callback) {
  console.log(data);
  req.app.pool.query('INSERT INTO guest(title, first_name, last_name, invite_id) VALUES ($1, $2, $3, $4)',
    [data.title, data.first_name, data.last_name, id], function (err) {
      if (err) {
        console.log(data);
        callback(err);
        return;
      }
      get_invite_guests(req, id, callback);
    });
};
const summary = function (req, callback) {
  req.app.pool.query('select count(case when is_going=true then 1 end) as going, count(case when is_going=false then 1 end) as not_going, COUNT(case when is_going IS NULL then 1 end) as not_responded from guest', function (err, result) {
    if (err) {
      callback(err);
      return;
    }
    callback(null, result.rows[0]);
  });
};
const check_code = function (req, data, callback) {
  if (typeof data.name == 'undefined') {
    callback('Missing code, can not check');
    return
  }
  req.app.pool.query("with found_invite as ( Select id as found_id from invite where code=$1) select * from guest, found_invite where invite_id=found_invite.found_id", [data.name], function (err, result) {
    if (err) {
      callback(err);
      return
    }
    if (result.rows.length == 0) {
      callback(null, {error: 'Invalid code'});
      return;
    }
    const returnVal = {
      guests: result.rows,
      error: null
    };

    if (result.rows[0].is_going != null) {
      returnVal.error = 'Already responded to invite'
    }
    callback(null, returnVal);
  })
};
const submit_rsvp = function (req, data, callback) {
  console.log(data);
  if (!Array.isArray(data) || data.length == 0) {
    callback('can not rsvp without guests!');
    return;
  }
  var names = "";
  var sqlCommand = "update guest as g set is_going = c.going from(values ";
  for (var i = 0; i < data.length; i++) {
    sqlCommand += util.format("(%d, %s)", data[i].id, data[i].is_going !== null && data[i].is_going);
    names += data[i].first_name + " " + data[i].last_name;
    if (i < data.length - 1) {
      sqlCommand += ", ";
      names += ", ";
    }
  }
  sqlCommand += ") as c(id, going) where c.id = g.id";
  req.app.pool.query(sqlCommand, function (err, result) {
    if (err) {
      console.log(sqlCommand);
      callback(err);
      return
    }
    req.app.pool.query("UPDATE invite set date_responded=now() where id=$1", [data[0].invite_id], function () {
      callback(null, result.rows);
    });
  });
  req.app.pool.query("SELECT * FROM apn_key_store", function (err, result) {
    if (err) {
      console.log("Error trying to get apn key stores");
      return;
    }
    req.app.apn.sendMessage(names, result.rows);
  })
};
const guest_summary = function (req, callback) {
  req.app.pool.query('SELECT * FROM invite INNER JOIN guest ON (guest.invite_id = invite.id) ORDER BY guest.invite_id',
    function (err, result) {
      if (err) {
        callback(err);
        return;
      }
      const response = {invites: []};
      var lastInviteId = -1;
      var inviteInfo;
      for (var i in result.rows) {
        if (!result.rows.hasOwnProperty(i))
          continue;
        const rowData = result.rows[i];
        if (lastInviteId != rowData.invite_id) {
          if (lastInviteId !== -1) {
            //noinspection JSUnusedAssignment
            response.invites.push(inviteInfo);
          }
          lastInviteId = rowData.invite_id;
          inviteInfo = {name: rowData.name, responded: rowData.date_responded, guests: []}
        }
        inviteInfo.guests.push({
          title: rowData.title,
          first: rowData.first_name,
          last: rowData.last_name,
          response: rowData.is_going
        });
      }
      callback(null, response);
    });
};
const add_apn_token = function (req, id, callback) {
  req.app.pool.query('INSERT INTO apn_key_store(id, time_submitted) VALUES ($1, now())', [id], function (err, result) {
    if (err) {
      console.log('error inserting apn id ' + id);
      callback(err);
      return;
    }
    callback(null, result.rows);
  })
};

module.exports = {
  init,
  summary,
  list_guests,
  delete_guest,
  update_guest,
  list_invites,
  create_invite,
  delete_invite,
  get_invite_info,
  get_invite_guests,
  create_invite_guest,
  check_code,
  submit_rsvp,
  guest_summary,
  add_apn_token
};
