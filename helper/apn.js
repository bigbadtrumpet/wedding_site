var apn = require('apn');
var apnConnection;

var handleFeedback = function(devices) {
  console.log("feedback callback " + JSON.stringify(devices));
  try {
    devices.forEach(function(item) {
      if(!item || !item.device || !item.device.token){
        console.log('COuld not understand item ' + JSON.stringify(item));
        return;
      }
      var deviceString;
      var token = item.device.token;
      if(token.type === "Buffer"){
        console.log("Parsing array buffer");
        var buffer = new Buffer(token.data);
        deviceString = buffer.toString("hex")
      }
      else {
        console.log("Using raw data");
        deviceString = token.data;
      }
      console.log('about to drop ' + deviceString)
      // Drop device string if older than item.time
      app.pool.query("DELETE FROM apn_key_store where id=$1 AND extract('epoch' from time_submitted) < $2", [deviceString, item.time], function(err, result){
        if(err){
          console.log('error dropping id named ' + deviceString);
          return;
        }
        if(result.rows.length > 0){
          console.log('succesfully dropped id named ' + deviceString)
        }
      })
    });
  } catch (error) {
    console.log('Error processing feedback ' + error);
  }
}

exports.sendMessage = function(message, data){
  console.log("Sending message \"" + message + "\" to data " + JSON.stringify(data));
  data.forEach(function(item) {
    var note = new apn.Notification();
    note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
    note.badge = 1;
    note.alert = "Recieved response from " + message;
    var myDevice = new apn.Device(item.id);
    apnConnection.pushNotification(note, myDevice);
  })
}

exports.init = function(app){
  var options = {
    "production": false,
    "cert" : process.env.CERT_PEM,
    "key" : process.env.KEY_PEM
   };
  apnConnection = new apn.Connection(options);
  app.apn = this;
  var feedbackOptions = {
      "production": false,
      "batchFeedback": true,
      "interval": 3000,
      "SSL" : process.env.USE_DB_SSL,
      "cert" : process.env.CERT_PEM,
      "key" : process.env.KEY_PEM
  };

  var feedback = new apn.Feedback(feedbackOptions);
  feedback.on("feedback", handleFeedback);
}
