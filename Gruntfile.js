module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {
            // this is the "dev" Sass config used with "grunt watch" command
            dev: {
                options: {
                    outputStyle: 'expanded',
                    // tell Sass to look in the Bootstrap stylesheets directory when compiling
                    includePaths: [__dirname + '/node_modules/bootstrap-sass/assets/stylesheets/']
                },
                files: {
                    // the first path is the output and the second is the input
                    'public/css/compiled.css': 'sass/mystyle.scss'
                }
            },
            // this is the "production" Sass config used with the "grunt buildcss" command
            dist: {
                options: {
                    outputStyle: 'compressed',
                    // tell Sass to look in the Bootstrap stylesheets directory when compiling
                    includePaths: [__dirname + '/node_modules/bootstrap-sass/assets/stylesheets/']
                },
                files: {
                    'public/css/compiled.css': 'sass/mystyle.scss'
                }
            }
        },
        // configure the "grunt watch" task
        watch: {
            sass: {
                files: 'sass/*.scss',
                tasks: ['sass:dev']
            }
        }
    });
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    // "grunt buildcss" is the same as running "grunt sass:dist".
    // if I had other tasks, I could add them to this array.
    grunt.registerTask('buildcss', ['sass:dist']);
};
