angular.module('rsvp', [])
.controller('mainController', function($scope, $http) {

  $scope.inviteFormData = {};
  $scope.inviteData = {};
  $scope.summaryData = {};

  $scope.createInvite = function() {
    $http.post('/api/v1/invite', $scope.inviteFormData)
        .success(function(data) {
            $scope.inviteFormData = {};
            $scope.inviteData = data;
            console.log(data);
        })
        .error(function(error) {
            console.log('Error: ' + error);
        });
  }
  $scope.initInvite = function(invite){
    invite.guestsGoing = function(){
      if(!Array.isArray(this.guests)){
        return -1;
      }
      var going = 0;
      for(i = 0; i < this.guests.length; i ++){
        if(this.guests[i].is_going == true) {
          going ++;
        }
      }
      return going;
    }
    $http.get('/api/v1/invite/' + invite.id + '/guest')
      .success(function(data){
        invite.guests = data;
      })
      .error(function(data){
          console.log('Error: ' + data);
      })

  }
  $scope.deleteInvite = function(inviteId){
    $http.delete('/api/v1/invite/' + inviteId)
        .success(function(data) {
          $scope.inviteData = data;
          $scope.refreshSummary();
        })
        .error(function(data) {
            console.log('Error: ' + data);
        });
  }
  $scope.toggleInvite = function(invite){
    invite.show = typeof invite.show === 'undefined' || !invite.show;
  }
  $scope.startEdit = function(guest){
    console.log('start edit');
    guest.edit = true;
  }
  $scope.refresh = function(guest){
    $http.get('/api/v1/invite')
        .success(function(data) {
            $scope.inviteData = data;
            console.log(data);
        })
        .error(function(error) {
            console.log('Error: ' + error);
        });
  }
  $scope.updateGuest = function(guest){
    guest.edit = false;
    $http.put('/api/v1/guest/' + guest.id, guest)
      .success(function(data){
        guest = data;
      })
      .error(function(data){
          console.log('Error: ' + data);
      })
  }
  $scope.deleteGuest = function(invite, guest){
    guest.edit = false;
    $http.delete('/api/v1/guest/' + guest.id)
      .success(function(data){
        invite.guests = data;
        $scope.refreshSummary();
      })
      .error(function(data){
          console.log('Error: ' + data);
      })
  }
  $scope.createGuest = function(invite){
    $http.post('/api/v1/invite/' + invite.id+ '/guest', invite.guest)
      .success(function(data){
        invite.guests = data
        $scope.refreshSummary();
      })
  }
  $scope.refreshSummary = function(){
    $http.get('/api/v1/summary')
      .success(function(data){
        $scope.summary = data;
      })
      .error(function(error){
        console.log('Error: ' + error);
      });
  }

  $http.get('/api/v1/invite')
      .success(function(data) {
          $scope.inviteData = data;
          console.log(data);
      })
      .error(function(error) {
          console.log('Error: ' + error);
      });
  $scope.refreshSummary();
});
