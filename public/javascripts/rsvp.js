angular.module('rsvp', [])
.controller('responseController', function($scope, $http) {
  $scope.code ={};
  $scope.guests = [];
  $scope.showRsvpPage = false;
  $scope.readonly = false;
  $scope.submitCode = function(){
    $http.post('/api/v1/check_code', $scope.code)
      .success(function(data){
        if(data.guests === undefined){
          alert('Invalid invite code, please check your code and try again!')
          return;
        }
        $scope.readonly = data.error !== undefined && data.error !== null;
        $scope.showRsvpPage = true;
        $scope.guests = data.guests;
      })
      .error(function(err){
        $scope.showRsvpPage = false;
        console.log('Error, invalid code');
      })
  };
  $scope.submitRsvp = function(){
    $http.post('/api/v1/submit_rsvp', $scope.guests)
      .success(function(data){
        $scope.readonly=true;
      })
  }
  $scope.allSelected = function(){
    if($scope.guests == null || $scope.guests.length == 0){
      return false;
    }
    for(i = 0; i < $scope.guests.length; i++){
      if($scope.guests[i].is_going === null){
        return false;
      }
    }
    return true;
  }
});
